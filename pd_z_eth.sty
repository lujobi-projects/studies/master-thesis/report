%==============================================================================
% ETHIDSC -- (ethidsc.sty) Style file
% A LaTeX package providing new style definitions and commands.
% The ethidsc-package defines a title page and an info page for term papers
% and master theses written at the Measurement and Control Laboratory
% of the Swiss Federal Institute of Technology (ETH) Zurich.
% Author: Eric A. Mueller
%         Measurement and Control Laboratory
%         Swiss Federal Institute of Technology (ETH) Zurich
%         Zurich, Switzerland
%%
% Copyright (c) 2004 Eric A. Mueller
% All rights reserved.
%
% ethidsc.sty,v.1.0 2009/29/05

% Revisions:  	 2011/03/22  (Soren Ebbesen)
%               2013/03/08  (Soren Ebbesen)
%
%==============================================================================
\def\Presentation@Version{1.1}
\typeout{providing new style definitions and commands ---}


\newif\ifingerman
\DeclareOption{ngerman}{\global\ingermantrue}
\DeclareOption{english}{\global\ingermanfalse}

\DeclareOption{mt}{\gdef\@type{\ifingerman Masterarbeit\else Master Thesis\fi}}
\DeclareOption{bt}{\gdef\@type{\ifingerman Bachelorarbeit\else Bachelor Thesis\fi}}
\DeclareOption{st}{\gdef\@type{\ifingerman Semesterarbeit\else Semester Thesis\fi}}

\ExecuteOptions{german,st}
\ProcessOptions


% Font improvement
\RequirePackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{textcomp}


% Econding
\RequirePackage[latin1]{inputenc}


% Math
\RequirePackage{amsmath,amssymb,amsthm}


%Graphics
\RequirePackage{graphicx}
\RequirePackage[dvips]{epsfig} 

% Graphics directly with TeX
\usepackage{tikz}


% Caption for Floating Bodies 
\usepackage{floatrow}
\floatsetup[table]{capposition=bottom}
\floatsetup[figure]{capposition=bottom}

% Tabellen	
\usepackage{array} % Dok: http://mirror.switch.ch/ftp/mirror/tex/macros/latex/required/tools/array.pdf

% SI-Einheiten
\usepackage{siunitx} 

% Zeilenumbruch
\sloppy

% Code listing
\usepackage{listings}		
\usepackage{array} 	

% Titleblatt
\usepackage{textpos}		% Textblock

% EPS to PDF
\usepackage{epstopdf}

\usepackage{microtype}
\DisableLigatures{encoding = *, family = * }

% Cite	
%\usepackage{cite} 
\usepackage[square,numbers]{natbib}

% Kopf-/Fusszeilen-Stil
\RequirePackage{fancyhdr}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\renewcommand{\sectionmark}[1]{\markright{\thesection.\ #1}}

\RequirePackage{multirow}
\RequirePackage{rotating}
\RequirePackage{mcode}				%for easy matlab integration
% \RequirePackage{units}
\RequirePackage{booktabs}
\RequirePackage{url}
\definecolor{pdz}{rgb}{0.05098,0.1137255,0.25098}

%Title Formatierung
\usepackage{color}
\usepackage{titlesec}
\titleformat{\chapter}[hang]{\normalfont\huge\bfseries}{\thechapter}{20pt}{\Huge}


% Macros - Customized commands
\input{style/def}

% Set language
\ifingerman
\RequirePackage{ngerman,ae}
\usepackage[ngerman]{hyperref} 
\else
\usepackage{hyperref} 
\fi

\newcommand{\@subtitle}{}
\newcommand{\subtitle}[1]{\gdef\@subtitle{#1}}

\newcommand{\@supervision}{}
\newcommand{\supervision}[1]{\gdef\@supervision{#1}}

\newcommand{\@nbsupervision}{}
\newcommand{\nbsupervision}[1]{\gdef\@nbsupervision{#1}}

\newcommand{\@autor}{}
\newcommand{\autor}[1]{\gdef\@autor{#1}}

\newcommand{\@startdate}{}
\newcommand{\startdate}[1]{\gdef\@startdate{#1}}

\newcommand{\@dateend}{}
\newcommand{\dateend}[1]{\gdef\@dateend{#1}}

\newcommand{\@professor}{}
\newcommand{\professor}[1]{\gdef\@professor{#1}}

\newcommand{\@nbprofessor}{}
\newcommand{\nbprofessor}[1]{\gdef\@nbprofessor{#1}}

\newcommand{\@identification}{}
\newcommand{\identification}[1]{\gdef\@identification{#1}}

\newcommand{\@titleimage}{}
\newcommand{\titleimage}[1]{\gdef\@titleimage{#1}}

\newcommand{\@declaration}{}
\newcommand{\declaration}{\gdef\@declaration{}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definition of title page

\definecolor{titlegrey}{gray}{0.2}		% Font color

\renewcommand{\maketitle}{
\pagestyle{empty}

%Top Line
\begin{textblock*}{30mm}[0,0](60mm,-50mm)
\noindent\makebox[\linewidth]{\textcolor{pdz}{\rule{25cm}{12pt}}}
\end{textblock*}

%Foot Line
\begin{textblock*}{30mm}[0,0](60mm,246mm)
\noindent\makebox[\linewidth]{\textcolor{pdz}{\rule{25cm}{5pt}}}
\end{textblock*}

\vspace*{-4.5cm}

\begin{center}

\parbox[c][\textheight][t]{0.97\textwidth}{
\begin{center}

\vspace*{-1cm}
\begin{minipage}[c]{0.4\textwidth}
 \begin{flushleft}
 \hspace*{-1.5cm}
 \includegraphics[height=1.5cm]{img/ethlogo.eps}
 \end{flushleft}
\end{minipage}
\hfill
\begin{minipage}[c]{0.4\textwidth}
 \begin{flushright}
 \hspace*{0.4cm}
 \includegraphics[height=2.5cm]{img/pd_z_logo.eps}
 \end{flushright}
\end{minipage}

% Line under PDZ
\begin{textblock}{1}(10,0.1)
\noindent\makebox[\linewidth]{\textcolor{pdz}{\rule{11cm}{4pt}}}
\end{textblock}

\end{center}
}
\end{center}
%

\begin{textblock*}{165mm}[0,0](-10.2mm,-200mm)

%Type of Work
\vspace{2cm}
\textcolor{titlegrey}{\large \bf \@type}\\[3ex]

%Title
\vspace{0.6cm} 
\textcolor{titlegrey}{\Huge \bf \@title}\\[3ex]

%Subtitle
\vspace{-0.5cm} 
\textcolor{titlegrey}{\Large \bf \@subtitle}\\[3ex]
\end{textblock*}

\begin{textblock*}{200mm}[0,0](-10.2mm,-127mm)


%Image
\vspace{0.5cm} 
\hspace{1cm}
\begin{minipage}[c]{1\textwidth}
 \hspace*{-1.3cm}
 \includegraphics[height=10cm, width=16.5cm]{\@titleimage}
\end{minipage}

% Autor
\vspace{0.5cm}
\ifingerman
 \textcolor{titlegrey}{\bf Autor} \\[1.5ex]
\else
 \textcolor{titlegrey}{\bf Author} \\[1.5ex]
\fi
\@autor

%Supervisor
\vspace{1cm}
\ifingerman
 \textcolor{titlegrey}{\bf Betreuer} \\[1.5ex]
\else
 \ifnum \@nbsupervision=0
 \textcolor{titlegrey}{\bf Supervisor} \\[1.5ex]
 \else
 \textcolor{titlegrey}{\bf Supervisors} \\[1.5ex]
 \fi
\fi
\@supervision

\vfill

%Professor
\vspace{1cm}
\ifingerman
\ifnum \@nbprofessor=0
\textcolor{titlegrey}{\bf Professor} \\[1.5ex]
\else
\textcolor{titlegrey}{\bf Professoren} \\[1.5ex]
\fi
\else
 \ifnum \@nbprofessor=0
 \textcolor{titlegrey}{\bf Professor} \\[1.5ex]
 \else
 \textcolor{titlegrey}{\bf Professors} \\[1.5ex]
 \fi
\fi
\@professor

%Identification and date
\vspace{1cm}
\texttt{Nr.\@identification} 
\hspace{11cm} 
\end{textblock*}

\begin{textblock*}{162mm}[0,0](-10.2mm,48mm)
\begin{flushright}
\@date
\end{flushright}
\end{textblock*}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Declaration Page

\renewcommand{\declaration}{
\pagestyle{empty}

%Top Line
\begin{textblock*}{30mm}[0,0](60mm,-50mm)
\noindent\makebox[\linewidth]{\textcolor{pdz}{\rule{25cm}{12pt}}}
\end{textblock*}

%Foot Line
\begin{textblock*}{30mm}[0,0](60mm,246mm)
\noindent\makebox[\linewidth]{\textcolor{pdz}{\rule{25cm}{5pt}}}
\end{textblock*}

\vspace*{-4.5cm}

\begin{center}

\parbox[c][\textheight][t]{0.97\textwidth}{
\begin{center}

\vspace*{-1cm}
\begin{minipage}[c]{0.4\textwidth}
 \begin{flushleft}
 \hspace*{-1.5cm}
 \includegraphics[height=1.5cm]{img/ethlogo.eps}
 \end{flushleft}
\end{minipage}
\hfill
\begin{minipage}[c]{0.4\textwidth}
 \begin{flushright}
 \hspace*{0.4cm}
 \includegraphics[height=2.5cm]{img/pd_z_logo.eps}
 \end{flushright}
\end{minipage}

% Line under PDZ
\begin{textblock}{1}(10,0.1)
\noindent\makebox[\linewidth]{\textcolor{pdz}{\rule{11cm}{4pt}}}
\end{textblock}

\end{center}
}
\end{center}

\begin{textblock*}{165mm}[0,0](-10.2mm,-200mm)

%Type of Work
\vspace{2cm}
\textcolor{titlegrey}{\large \bf \@type}\\[3ex]

%Title
\vspace{0.6cm} 
\textcolor{titlegrey}{\Huge \bf \@title}\\[3ex]

%Subtitle
\vspace{-0.5cm} 
\textcolor{titlegrey}{\Large \bf \@subtitle}\\[3ex]
\end{textblock*}

\begin{textblock*}{165mm}[0,0](-11mm,-130mm)
%Dates
\vspace{1.5cm} 
\ifingerman
 \textcolor{titlegrey}{\Large \bf Daten} \\[1.5ex]
\else
 \textcolor{titlegrey}{\Large \bf Dates} \\[1.5ex]
\fi

\vspace{-0.5cm} 
\ifingerman
{ Startdatum \textnormal{\@startdate}\hspace{5cm}Enddatum \textnormal{\@dateend}} \\[1.5ex]
\else
{ Start date \textnormal{\@startdate}\hspace{5.3cm}End date \textnormal{\@dateend}} \\[1.5ex]
\fi

%Declaration
\vspace{1cm}
\ifingerman
\textcolor{titlegrey}{\Large \bf Eigenst\"{a}ndigkeitserkl\"{a}rung } \\[1.5ex]
{Ich best\"{a}tige, die vorliegende Arbeit selbst\"{a}ndig und in eigenen Worten verfasst zu haben. Davon ausgenommen sind sprachliche und inhaltliche Korrekturvorschl\"{a}ge durch den Betreuer der Arbeit. Es wurden keine weiteren Quellen oder Hilfsmittel verwendet, als die in der Bibliographie Angegebenen.}\\
\else
\textcolor{titlegrey}{\Large \bf Declaration of Originality} \\[1.5ex]
{I hereby declare that I have written the present thesis independently and have not used other}\\ {sources and aids than those stated in the bibliography.}
\fi

%Signature of student
\vspace{1cm}
\hspace{-4.4cm} %To adapt if size of box is changed
\noindent\makebox[\linewidth]{\textcolor{black}{\rule{8cm}{0.5pt}}}

\hspace{-0.15cm}
\vspace{0.5cm}
\@autor


%Confirmation
\vspace{1cm}
\ifingerman
\textcolor{titlegrey}{\Large \bf Best\"{a}tigung } \\[1.5ex]
{Hiermit wird best\"{a}tigt, dass die Arbeit in der \pdz \, durchgef\"{u}hrt und durch diese angenommen wurde.}\\
\else
\textcolor{titlegrey}{\Large \bf Confirmation} \\[1.5ex]
{This thesis was written at and accepted by \pdz \, Product Development Group Zurich of ETH Zurich.}
\fi

%Signatures of professor and supervisor
\vspace{1cm}
\ifingerman
\ifnum \@nbprofessor=0
\textcolor{titlegrey}{\Large \bf Betreuer\hspace{6.5cm}Professor}\\[1.5ex]
\else
\textcolor{titlegrey}{\Large \bf Betreuer\hspace{6.5cm}Professoren}\\[1.5ex]
\fi
\else
\ifnum \@nbprofessor=0
\textcolor{titlegrey}{\Large \bf Supervisor \hspace{5.7cm}Professor} \\[1.5ex]
\else
\textcolor{titlegrey}{\Large \bf Supervisors \hspace{5.5cm}Professors} \\[1.5ex]
\fi
\fi

\vspace{1cm}
\hspace{-4.6cm} %To adapt if size of box is changed
\noindent\makebox[\linewidth]{\textcolor{black}{\rule{7.5cm}{0.5pt}}}

\vspace{-0.5cm}
\hspace{4.5cm} %To adapt if size of box is changed
\noindent\makebox[\linewidth]{\textcolor{black}{\rule{7.5cm}{0.5pt}}}

\end{textblock*}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Seitenr\"ander
\usepackage{calc}
\usepackage{layout}
%\usepackage{layouts}

\addtolength{\headheight}{2.5pt}
\addtolength{\footskip}{-30pt}
\addtolength{\headsep}{10pt}
\addtolength{\topmargin}{-10pt-2.5pt}
\addtolength{\textheight}{30pt-10pt+10pt}
