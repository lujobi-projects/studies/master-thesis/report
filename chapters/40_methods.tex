\chapter{Methods}\label{ch:methods}
To answer the research questions (see \cref{sec:ReasearchQuestion}) and to evaluate the chances of the VR app created during this thesis was conducted with the maintenance workers. This study presents the app to technicians with different  experiences in coupling maintenance to determine the acceptance and the user's opinion. The documents presented to the workers can be found in \cref{app:userstudy}.

\section{Intent}
This user study intends to give answers to the Research Questions (see \cref{sec:ReasearchQuestion}) and collect the relevant data while keeping the impact on RICO's daily work process as low as possible. In other words, the test duration has to be kept as low as possible but long enough to give the user enough time to accommodate in the VR world while collecting as much feedback and data as possible. To further reduce the impact on the maintenance center, the RICO asked to keep the number of test subjects low. 


\newcommand{\sogroup}[0]{\textbf{$SG\rightarrow Occ$}}
\newcommand{\osgroup}[0]{\textbf{$Occ\rightarrow SG$}}

\section{Study Design}
To answer \cref{rq:htSG} the subjects were divided into two groups: one started with the SenseGloves the other started with the Oculus Quest's hand-tracking. These groups are referred to as \sogroup{} and \osgroup{} respectively. 
\begin{table}[htb]
    \centering
    \begin{tabular}{|l||l|l|}
        \hline 
         & Part 1 & Part 3 \\ \hline \hline
        \sogroup{} & SenseGloves & Meta hand-tracking \\ \hline
        \osgroup{} & Meta hand-tracking & SenseGloves \\ \hline
    \end{tabular}
    \caption{The two test groups and which type of hand-tracking to use}
    \label{tab:testing-order}
\end{table}


% \section{Procedure during the study}
% This section outlines the planned proceedings of the user study as well as the ideas and decisions that lead to said procedure. 

\subsection{Setup}
The study requires a free room of approximately 2 by 3 meters, the headset, and the SenseGloves. However, running the app natively on the headset does not yield an optimal user experience as the headset's performance did not fully stand up to the task  (see \cref{sec:performance}). For that reason, the Meta Quest was connected to the computer via the Link cable. Furthermore, using the computer allows the study supervisor to see what the user does from an outside perspective through the Unity Editor. Before every test, the connection to the SenseGloves and the connection via the Link Cable are tested. Right before the tests, Meta released an update that automatically switches to hand-tracking when the controllers and, with that, the SenseGloves are put on a still surface. Thus no other Setup was needed to facilitate switching between the input systems. This setup is depicted in \cref{fig:studysetuphardware}.

\begin{figure}[htb]
    \centering
    \includegraphics[width=.7\textwidth]{img/40_methods/HardwareSetup.png}
    \caption{The Hardware Setup used during the User Study}
    \label{fig:studysetuphardware}
\end{figure}


\subsection{Task}\label{ssec::methodstasks}
The branches Part 1 (named \textit{Teil 1} in the app) and Part 3 (resp. \textit{Teil 3}) are the branches of the decision tree (i.e., the manual) used during the test as they have been fully implemented and animated (see \cref{sec:stateofanim}). Additionally, they contain similar steps: at the beginning of both branches, the user has to open up the electronic box. Then, they diverge, giving the user some variety and letting them use different tools. These tasks were solved in subsequent order by both \sogroup{} and \osgroup{} as indicated in \cref{tab:testing-order}. 


As seen during the pre-studies, most participants had never used a VR app before and were eager to test out interaction in the VR world. That is why the user's first step in the study was to accommodate themselves within the VR world and "play around" within the app. Afterward, the scene was reset, and the actual study started.


\subsection{Procedure}
Once all preparation has been conducted, the study looked as follows:
\begin{enumerate}
    \item Sitting down with the participant and introduce them to the experiment:
        \begin{enumerate}
            \item Short personal introduction 
            \item Some security information
            \item Overview over the goal of this study w.r.t. this thesis.
            \item Introduction to the two different input systems
            \item Explanation of the schedule of the study, pictures of where to find what in the VR world (see pictures in \cref{app:userstudy}) 
            \item Explanation and signing of the declaration of consent
        \end{enumerate}
    \item Set up the first input system and let the user accommodate within the VR World
    \item Reset the scene and start with Part 1 of the manual
    \item Remove the headset, sit down with the user and fill in the respective questions.
    \item Set up the second input system and start with Part 3 of the manual
    \item Remove the headset, sit down with the user and fill in the respective questions as well as the open questions.
\end{enumerate}

\subsection{Questionnaire}\label{ssec:questionarrire}
The content questionnaire is partially based on Mosberger \cite{AntoniaMT} planning to compare the answers of this user study with the results from the previously conducted ones, which would give an answer to \cref{rq:ARvsVR}. Before starting the first tests, personal data was collected, e.g., experience with the coupling, age in service at RICO, and native language.

The first questions are based on the so-called System Usability Scale \cite{SusQuickandDirtyUsabilit}, a set of predetermined questions to efficiently evaluate the usability of a system. Whereas the definition of the system can be determined freely. These questions have been translated to German accordingly by Rauer \cite{SusQuantitaveAnalysen}. Additionally, some questions from NASA Task Load Index \cite{NASATLX} - another set of predetermined, standardized questions - determine the cognitive load while using the app. The participants were asked to answer the questions for both hand-tracking variants to answer \cref{rq:ARvsVR}. Those so-called \textit{short questions} are answered on a scale from 1 to 5, representing \textit{fully disagree} to \textit{fully agree}.

\subsubsection{Open Questions}\label{ssec:openquestions}
Several open questions intending to answer \cref{rq:acceptance} and give more insights into \cref{rq:ARvsVR,rq:htSG} complete the questionnaire. They target the general feeling within the app, the interactions with the model, and the sequence of tasks. Furthermore,  the questions intend to compare the SenseGloves and the Meta Quest's hand-tracking. Having spent quite some development time on the simulation of the screw (see \cref{eq:screwingmotion}), the participants were asked what they think of that simulation, giving first answers to \cref{rq:neededexactness}. At last, it asks the ones who had tested the AR-googles whether the VR app gives a better understanding of the overall system. 

The following questions were asked:
\begin{enumerate}
    \item General Feeling: What do you think of the VR-app? How did it feel to conduct the diagnosis in the virtual world?
    \item How did the interactions with the model feel?
    \item What do you think of the SenseGloves? Do they add some value? Which input system felt more "{}exact"{}?
    \item What do you think of the simulation of the screws? (necessary/realistic)
    \item (Only if the AR-app has been tested) Do you think that the VR-app brings a better understanding for the overall system?
    \item An empty box box to note other remarks and feedback.
\end{enumerate}


\subsubsection{Participants} \label{ssec:participants}
This study aims to gather answers from the complete demographic of technicians at RICO. As such, the study was conducted with various participants: from apprentices to experts, from computer beginners to advanced users or gamers, and from teenagers to over forty-year-olds. All participants were male. The demographics are evaluated using the personal data from the questionnaire (see \cref{ssec:questionarrire}).


\subsubsection{Evaluation}
In previous studies \cite{SilasMT, AntoniaMT}, the authors were told that reading and understanding the questions were, at times, the more significant challenge to the participants than performing the task in the AR app itself. Furthermore, a lot of feedback and answers to the open questions were given in a more informal setting. That is why the open questions, as well as the short questions, were answered in a relatively informal conversation with the users between the tests asking them about their experience using the app.

