\chapter{Introduction}\label{ch:introduction}

\section{Motivation} \label{sec:motivation}

For most technical systems, maintenance is a big part of the operating cost, frequently surpassing the procurement cost. For example, the train FV-Dosto - purchased SBB in 2010 - for 1.9 billion CHF \cite{SBBDosto}, and its maintenance costs are estimated to be around 3 billion CHF \cite{SBBmaintcostssDosto} - 150 percent of the procurement costs. While this might be a particular case, the Deutsche Bahn alone occupies over 11.000 employees in the maintenance area. Within the European Union, 16 billion EUR are annually spent on vehicle maintenance, 3.3 billion alone in Germany \cite{OnTheDigitalTrack}. As we deal with such a massive volume, the potential of even small optimization is significant. The annual reports by Unife (European Rail Supply Industry Association) mention several (many of them perennial) projects which promise reductions in overall maintenance costs, which are currently being worked on \cite{UnifeAnnualReport2019, UnifeAnnualReport2020, UnifeAnnualReport2021}.

Rolling stock maintenance is hard. It is hard to master these complex systems - even a small module of the train, like the coupling, the door, and the toilets, are very intricate. Also, the job as a maintenance worker is physically demanding. Maintenance work is conducted in shifts as the trains are mostly free at night. Such challenging work conditions contribute to the high staff turnover experienced at RICO and other similar maintenance facilities.

VR and AR cannot mitigate this high staff turnover. However, using AR as an interactive manual allows less experienced workers to work on a new module earlier \cite{AntoniaMT, SilasMT}. If a worker needs less time in training and can directly and independently start to work on the system, this results in a cut in costs while conserving another resource: the expert's time. The experts introduce a new worker to a module. As the technician's experience with such a module grows, the experts teach them more complicated tasks e.g., how to locate an error within that module. 

\clearpage

The "Regionales Instandhaltungscenter Ostschweiz" (RICO) was \pdz{}' industry partner for various past theses \cite{AntoniaMT, SilasMT, IanHutterST}. RICO is the maintenance center where most of the Thurbo fleet \cite{thurbo} is processed. As done during previous theses, this thesis will perform its research on the coupling of the Thurbo train. It is among the top three error-prone systems within the current fleet of the train, among the doors and the air conditioning and heating system. The coupling in and of itself is a well-contained system; Nonetheless, it is essential for the operation of the train. The previous work focused on preemptive checkups and troubleshooting of the coupling \cite{AntoniaMT, SilasMT, IanHutterST}. These two processes are good examples of complex maintenance tasks within a well-defined, manageable system. To create their AR apps, previous theses  determined a decision tree to be the most suitable representation for such complex (maintenance) tasks as they cannot be depicted as a linear process \cite{AntoniaMT, SilasMT, IanHutterST}. Such a decision tree is depicted in \cref{fig:complexmaintentancetask}. 

\begin{figure}[htb]
    \centering
    \includegraphics[width=.7\textwidth]{img/appendix/CompleteTree}
    \caption{The decision tree of the preemptive checkup task, \treesource{}}
    \label{fig:complexmaintentancetask}
\end{figure}

During the evaluation phase of the theses mentioned above \cite{AntoniaMT, SilasMT}, the workers that tested the AR app agreed that it is suitable as a different form of a manual \cite{SilasMT} and would mainly be helpful when working on the coupling for the first time. The workers agree that these AR apps do not replace an instructor as they - for example - do not pose any questions based on how the user is acting \cite{SilasMT}. Furthermore, they mentioned that they need help understanding why they are doing the current step w.r.t. to some bigger picture \cite{AntoniaMT}. 

Introducing a VR app into the training of maintenance workers might bring the following added value to the RICO: 
\begin{compactitem}
    \item \textbf{Standardisation:} Even though there is a service manual, it leaves some leeway in how to proceed when diagnosing the coupling. Thus, the ways of the single experts differ in quite some points \cite{AntoniaMT} and will diverge more over time. A VR training app can enforce one standardized procedure for every trainee. 
    \item \textbf{Fewer Experts needed:} Experts and their time are rare. With such a VR app, the need for expert time can be significantly reduced, especially in the labor-intensive part when getting to know a new module. Also, one expert can supervise several technicians once if one central system collects the data.
    \item \textbf{No Vehicle needed:} As no actual vehicle is required, the trainee does not interfere with the maintenance process. Errors can be simulated directly in the AR app and do not need an expert intentionally breaking a part of the coupling, which might be hard to fix later (as seen in \cite{AntoniaMT}). Furthermore, given a good enough simulation - the mishandling of devices and the coupling can be simulated, and the technician does not have to fear breaking anything.
    \item \textbf{Economic Benefits:} At times, there is not much going on at RICO. Participants can use these downtimes to train in VR, even if no train is there. Also, once the trainees work on the actual system, they have their first experience with any of these tasks. Thus, they will block the train for shorter time, potentially decreasing the maintenance window. 
    \item \textbf{Support different Learning Styles:} A VR app can support all different learning types: Visual learning, Aural learning, reading(/writing) learning, and Kinesthetic learning \cite{fleming2006learning}. Such a broad spectrum will improve the average learning time.
    \item \textbf{Context Awareness:} A VR app is completely context-aware, as everything is happening within that simulation. As such, the app knows about the current state of the coupling and can leave the user more freedom in how they approach a particular task, allows them to explore the environment by themselves, and might even be able to ask "clever" questions based on the user's progression.
    \item \textbf{Automatic Reports and fast Feedback:} As such, a VR app can precisely track every movement and interaction within the VR world. Thus it can give the user feedback quickly and to a custom degree of detail. Also, this information can be used to generate automatic reports on the training performance and progress, which can then be used to support the trainees individually.   
    \item \textbf{Understanding of the overall System:} Above mentioned context awareness allows the user to explore the VR and coupling system freely and then give them fast and detailed feedback, allowing the user to learn at his rate, find out about complex relations, and build a better understanding of the entire system. 
\end{compactitem}


Several companies created haptic feedback equipment to increase the immersiveness of VR apps. Most important are the haptic feedback gloves, which let the user feel the interaction with the objects within the VR world. Some notable examples are HaptX\cite{hfglove_haptx}, teslasuit
\cite{hfglove_teslasuit}, VRgluv\cite{hfglove_vrgluv} and SenseGloves\cite{hfglove_sg} of which the SenseGloves are available for this thesis. To increase the immersiveness even further, several approaches to provide haptic full-body suits as well as omnidirectional thread mills have been presented \cite{fullbodyhapt_virtuix, fullbodyhapt_katvr}. The first usages for such haptic devices were military and surgery training. However, over the past decade, these devices have been commercialized enough to find more and more usage in industrial applications \cite{SGVWClientcase}.


\section{Related Work}
In the past decade, VR has increasingly established itself as means of training, especially in surgical procedures. In literature, the term \textit{VR} has been stretched extensively and ranges from an interactive computer app to VR headsets. As such, "VR" apps have been studied for several decades. The most promising results stem from the past decade. Also, haptic feedback gloves and several other haptic devices and their effects on learning progress have been broadly studied. However, in conjunction with VR (headset) training, haptic feedback devices have been studied relatively sparsely - apart from some proof of concepts in clinical training.

\subsection{VR in (Maintenance-) Training}
As with many modern technologies, VR applications found their first usages in astronomy, and the military  \cite{historyofVRAstronomy}. With VR hardware becoming increasingly commercialized, training in VR became an exciting teaching method in other areas. Since developing such an app is still rather elaborate \cite{Bo_2012}, it is mainly limited to areas where a mistake is either very costly, unsafe, or unethical. 

Pantelidis \cite{reasonstouseVRwithdeterminigmodel} presents a decision model showing when the usage of a VR training app is reasonable: First, the objective of the training needs to be defined. Based on this, it provides several indications towards and against VR training which are evaluated. This process not only answers whether VR is suitable but also outlines the requirements of the components involved in the simulation.  

% \iltodo{something on gamification}

As previously mentioned, clinical procedures are the most researched (and published) area of VR applications. 
Boejen \cite{BOEJEN2011185} describes the application in training teams of radiologists. One such example is described in Phillips et al. \cite{Virtualrealitytrainingforradiotherapybecomesareality} and shows "that the three-dimensional understanding of
anatomy and dose distributions is improved" \cite{BOEJEN2011185}. Gupta et al. \cite{OrthopedicSurgicalProcess} created a setup to prepare for orthopedic surgery on the femur,  training subjects for new surgical procedures. Based on an expert surgeon's feedback, the participants with a mean experience of over twelve years in the field all performed well and excellently in the simulator after training in a VR app. Two-thirds of them had never performed that exact surgery. Another noteworthy example is Besndea et a.\cite{ATN-MAME1524}, which has also implemented haptic feedback gloves for medical training. This work will be further discussed in \cref{sec:literaturehaptfbsys}.
% \iltodo{\cite{VirtualrealityitsmilitaryUtility}}

Over time the industry showed great interest in VR training apps. Ranging from apps to prevent accidents and raise awareness for the dangers in mining \cite{accidentsInmining} to the aviation \cite{AugmentedandVirtualforInspectionandMaintenanceProcessesintheAviationIndustry} and the verification of assembly and maintenance tasks \cite{Virtualrealityasatoolforverificationofassemblyandmaintenanceprocesses}. 
Eschen \cite{AugmentedandVirtualforInspectionandMaintenanceProcessesintheAviationIndustry} discusses an area of application on which this thesis will not elaborate further. Nonetheless, it is an area in maintenance with immense potential: Inspection of the hard-to-reach regions in a plane through VR. An inspector of complex surfaces greatly benefits the presentation using a VR headset. Van Wyk and de Villiers \cite{accidentsInmining} shows a broad acceptance among the mining workers and an improved safety culture, thanks to VR security training. Moreover, they line out the same indicators for using a VR app as Pantelidis\cite{reasonstouseVRwithdeterminigmodel}. Gomes de Sá et al. \cite{Virtualrealityasatoolforverificationofassemblyandmaintenanceprocesses} use VR before even training the technicians to verify that maintenance is doable already in the design phase of the product as well as to ascertain and optimize a maintenance procedure. 


The learning effect of VR is a topic that has to be explored further. Only around thirty percent of studies analyzed by Checa and Bustillo \cite{Areviewofimmersivevirtualrealityseriousgamestoenhancelearningandtraining} demonstrate enhanced learning and training performance. Up to ten percent see no added value compared to conventional learning methods. Especially the older studies like Kozak et al. \cite{noTransferfromVRtoRealworld} (from 1992) did not show any retention from virtual training and questioned the effect thereof. So does another study using a "VR" interaction rig and a PC-display in Gavish et al. \cite{Evaluatingvirtualrealityandaugmentedrealitytrainingforindustrialmaintenanceandassemblytasks} (from 2013).
On the one hand, the effects of training in VR go much further: as outlined in Pallavicini et al. \cite{militaryStressmanagement}, such activity can train the stress management of the user (in military usage), increasing their resilience to stress. That will then also positively affect the performance of the individual outside of the VR world. On the other hand, in the past decades, massive improvements to VR technology have been achieved such that the latest studies show much more promising results. Sattar et al. \cite{Sattar2019-jq} show that VR is the best learning method for medicine students in comparison to video-based, and text-based methods. Not only was the motivation with VR the highest, but so was the Learning Competency.

\subsection{Haptic Feedback Systems}\label{sec:literaturehaptfbsys}
As per Stone \cite{Haptic_feedback_a_brief_history}, haptic feedback has been present even before the breakthrough of VR in our current understanding. It was first used in sub-sea operations and nuclear facilities in telerobotic actuators. These actuators evolved increasingly into purpose-built contraptions used in conjunction with some predecessors of VR headsets. Similarly, countless variants of simulators for trains, planes, tanks, etc., have been created and used for training purposes. The success thereof is shown in many areas of application: e.g., Lathe operation \cite{SignificanceofHapticFeedbackinLearningLatheOperatingSkills}, surgical procedures \cite{Escobar-Castillejos2016}, and carpentry \cite{Hapticsenhancedmultitoolvirtualinterfacesfortrainingcarpentryskills}.

Strictly speaking, simulators are also some form of VR device. However, these simulators are not as versatile as the haptic VR equipment nowadays since the interactions can be based on any digital model and are then calculated in real time. On the other hand, haptic feedback gloves and haptic suits cannot (yet) simulate the exact force load on the body. 
% \todo{Comparision of VR with haptic glove vs. traditional simulator would be amazing, doesn't exist though}

The development of haptic gloves started with the Sayre Glove in 1977, a device merely used as a new input system without any haptic capabilities \cite{sturman1994survey}. Over time these \textit{wired gloves} evolved increasingly with more precise sensors \cite{sturman1994survey}. Around the end of the last millennium, the first aspirations included mechanical actuation to provide tactile and forced feedback to the user. These first approaches use - compared to today - rather rudimentary pneumatic actuators  controlled by an external device \cite{MasterIInewdesignforcefeedbackglove}. Over time several approaches have been tried out: electromagnetic brakes for every link \cite{hapticGloveWithMRBrakesforVirtualReality}, twisting strings with electromotors \cite{TwistedStringActuationSystem}, and more compact pneumatic systems\cite{perret2018commercial}. Nowadays, several vendors are providing commercial products with different features employing various technologies \cite{perret2018commercial}.
   
Morris et al. \cite{HapticFeedbackEnhancesForceSkillLearning} show that a mixture of haptic and visual feedback is the most efficient way of learning a task. Even though only demonstrated on a simple "follow the line task," it shows that "recall following visuohaptic training is significantly more accurate than recall following visual or haptic training alone, although haptic training alone is inferior to visual training alone" \cite{HapticFeedbackEnhancesForceSkillLearning}. Similarly, O'Malley et al. \cite{SharedControlinHapticSystemsforPerformanceEnhancementandTraining} show improvements in learning patterns of motion using force feedback in conjunction with visual clues over only visual clues. 

\clearpage

During the experiments of Yuksel et al. \cite{Visuohapticstudentslearningoffrictionconcepts}, they proved that a visuohaptic approach improved the student's reasoning over a particular concept. They indicated that the visuohaptic approach enhances students' understanding of a particular concept.
% Additionally, \cite{KASSUBA201359} shows that the visual part is still the most important in, e.g., object recognition. 
In their case studies with the Volkswagen Group \cite{SGVWClientcase} and the Royal Netherlands Army \cite{SGArmyClientcase}, SenseGlove describes that "One hundred percent of the trainees who completed the T6 vehicle assembly training found it more realistic to work with SenseGlove Nova versus the controller[...]"\cite{SGVWClientcase}. However, there have been no comparisons with visual hand-tracking. 

While the haptic gloves in Besnea et al. \cite{ATN-MAME1524} do not have a system to block the user's hand completely, they created a cost-effective glove whose use is demonstrated on a training app for medical training. However, the success thereof and the state of this training have yet to be confirmed.

Apart from the obvious applications in improving  simulating  force feedback equipment, another important use case is rehabilitation. Demain \cite{psychophysicalhanddevicesrehabilitation} shows that force feedback helps to stimulate hands affected by central nervous system disorders. Feintuch et al. \cite{feintuch2006integrating} suggest that it enhances success during a clinical intervention.


% \iltodo{The Visual, the Auditory and the Haptic - A User Study on Combining Modalities in Virtual Worlds}
% \iltodo{INTEGRATION OF A HAPTIC GLOVE IN A VIRTUAL REALITY-BASED ENVIRONMENT FOR MEDICAL TRAINING AND PROCEDURES }

% \subsection{Digital twin}

% --> Maybe ask M. Thalmann 


% \subsection{Contribution}
\section{Research Questions} \label{sec:ReasearchQuestion}
As it turns out, creating VR is an elaborate process. As such, this thesis contributes a proof of concept for the VR simulation of train coupling maintenance. This app was first and foremost created to answer the following questions:

\newcommand{\rqARvsVRText}[0]{
Is the VR app able to convey a better understanding of the overall system compared to a similar AR app?
}
\newcommand{\rqhtSGText}[0]{
Optical Hand-Tracking versus Haptic Feedback Gloves (from SenseGlove). Which one is better suited for such a training app?
}
\newcommand{\rqneededexactnessText}[0]{
How accurate does a Simulation need to be? Which areas are essential to a technician?
}
\newcommand{\rqacceptanceText}[0]{
How accepting are the maintenance worker towards a VR Training app? What chances do they see for such an app?
}

\begin{researchquestion}\label{rq:ARvsVR}\rqARvsVRText{}\end{researchquestion}
\begin{researchquestion}\label{rq:htSG}\rqhtSGText{}\end{researchquestion}
\begin{researchquestion}\label{rq:neededexactness}\rqneededexactnessText{}\end{researchquestion}
\begin{researchquestion}\label{rq:acceptance}\rqacceptanceText{}\end{researchquestion}

