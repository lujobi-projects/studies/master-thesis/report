
\chapter{Apparatus}\label{ch:apparatus}

\section{The Meta Quest 2} \label{sec:theMetaQuest}
The \textit{Meta Quest 2} (initially sold under the name \textit{Oculus Quest 2}) is produced by Reality Labs \cite{RealityLabs} founded in 2012 and acquired by Facebook in 2014 \cite{wiki:Reality_Labs}. Like its predecessor, the \textit{Oculus Quest}, the \textit{Quest 2} uses an insight tracking approach based on 4 (IR-) cameras. Meaning the device does not depend on any data delivered by an external sensor. It uses an algorithmic approach to determine the current pose in the room, so-called Simultaneous Localization and Mapping (SLAM) \cite{insight_tracking}. Given the (known) camera calibration, new key points in the room are mapped and then used to determine the pose of the Headset within said room. 

\begin{figure}[htb]
    \centering
    \includegraphics[width=.9\textwidth]{img/20_system/Meta_Quest_Front.png}
    \caption{Meta Quest 2 and its Controllers, Source \cite{TheWildMeta}}
    \label{fig:quest_front}
\end{figure}

\subsection{Technical Specification}

\begin{compactdesc}
\item[Chip:]Qualcomm Snapdragon XR2 with 6 GB RAM
\item[Storage:] 128 GB, 256 GB
\item[Display:] Fast-switch LCD 1832 x 1920 per eye at 60, 72, 90 Hz
\item[Operating System:] Android 10
\end{compactdesc}
Data from Meta \cite{Quest2Specs}.

\subsection{Different Vendors}
Apart from the Meta Quest 2, there are various vendors with several models of VR Headsets. An incomplete list of standalone, inside-out Headsets is depicted below:
\begin{compactitem}
    \item Pico with Pico Neo Series.
    \item HTC with HTC Vive Series
    \item Qualcomm
    \item Lenovo
    \item Google Daydream
\end{compactitem}

As the SenseGloves are an integral part of this Thesis and the SBB supplied a version containing a mounting rig for the Meta 2's Controllers, the Meta Quest 2 was an obvious choice as the primary device. However, for the future, e.g., the Pico Neo 3 or the Pico 4 Enterprise with its optional eye-tracking camera could be a valuable improvement. 


\section{Interaction within the VR world}
For an immersive experience in a 3D-VR World, it does not suffice to have a headset or similar providing stereo vision and mapping the user's head pose into the world. Moreover, it is essential to be able to interact with objects within that very world. Thanks to advancements in machine learning and vision algorithms and the availability of performant small-factor chips, these interactions calculated directly on the device were improved massively in the past years. 

\paragraph{Interaction using Controllers}
As mentioned in \cref{par:CtrTracking}, tracking of the controller is comparably simple and was thus the first widespread variant to interact with objects in VR. An object can be grabbed by pressing the buttons attached to the controller. This, however, leads to rather gross motor interactions.

Properties of the controller as an input device:

\begin{compactitem}
    \item Exact position
    \item Simple Grab
    \item No Pinching
\end{compactitem}


\begin{figure}[htb]
     \centering
     \begin{subfigure}[htb]{0.49\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/20_system/ht_evolution/world_ctr.png}
         \caption{Controllers in the 3D World}
         \label{sfig:worldCtr}
     \end{subfigure}
     \hfill
     \begin{subfigure}[htb]{0.49\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/20_system/ht_evolution/hands_ctr.jpg}
         \caption{Outside view}
         \label{sfig:handsCtr}
     \end{subfigure}
     \caption{Controller interaction}
     \label{fig:CtrInteraction}
\end{figure}

\paragraph{Interaction using Hand-Tracking}

With the help of cameras mounted to the VR headset, the movement of the hands can be tracked. Thanks to elaborate vision algorithms and AI, the headset can extract the poses of every link of the hand. These algorithms generate a hand skeleton that defines how the hand model is displayed

\textbf{Note:} This section discusses the Hand-Tracking system of the Meta Quest 2 exclusively. There are different hand-tracking approaches, e.g., the Microsoft Hololens. They more or less work in the same fashion and mainly differ in (setup-)details.

Properties of Hand-Tracking as input device:

\begin{compactitem}
    \item Natural, exact Grab-Model
    \item Prerecorded Handposes
    \item Precise Pinching
    \item Limiting the number of poses
\end{compactitem}


\begin{figure}[htb]
     \centering
     \begin{subfigure}[htb]{0.49\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/20_system/ht_evolution/world_ht.png}
         \caption{Handtracking in the 3D World}
         \label{sfig:worldHt}
     \end{subfigure}
     \hfill
     \begin{subfigure}[htb]{0.49\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/20_system/ht_evolution/hands_ht.jpg}
         \caption{Outside view}
         \label{sfig:handsHt}
     \end{subfigure}
     \caption{Hand-tracking}
     \label{fig:HTInteraction}
\end{figure}

\paragraph{Haptic Feedback Gloves}

Haptic feedback Gloves use some string or hydraulic system to restrict the finger's movement. As the AI models used to track the hands are usually proprietary and/or not too well adjusted to track hands in gloves, haptic feedback gloves depend on some externally mounted device such that they can be tracked. This tracker could be the controller as it is with the quest or a dedicated device like the HTC Vive tracker.


\textbf{Note:} This section discusses the SenseGlove haptic feedback gloves exclusively. There is a variety of different vendors. They more or less work in the same fashion and primarily differ on a technical level on how the restriction of hand movement is achieved.

Properties of Hand-Tracking as input device:

\begin{compactitem} 
    \item \textbf{Haptic Feedback!}
    \item Infinite GrabPoints, built-in Physics
    \item Not well suited for fine motor tasks
    \item Heavy Gloves
    \item GrabPoints based on Colliders
\end{compactitem}

\begin{figure}[htb]
     \centering
     \begin{subfigure}[htb]{0.49\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/20_system/ht_evolution/world_sg.png}
         \caption{SenseGloves in the 3D World}
         \label{sfig:worldSg}
     \end{subfigure}
     \hfill
     \begin{subfigure}[htb]{0.49\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/20_system/ht_evolution/hands_sg.jpg}
         \caption{Outside view}
         \label{sfig:handsSg}
     \end{subfigure}
     \caption{Haptic Gloves}
     \label{fig:SGInteraction}
\end{figure}


\paragraph{Small Aside on Controller Tracking}\label{par:CtrTracking} Different from hand-tracking, tracking the controller is usually done in the same way as the SLAM approach. The controller has IR-LEDs in a known pattern attached mostly to the prominent arc on top of the controller. These LEDs are then tracked using the same cameras as used by the localization \cite{insight_tracking}. As long as the cameras can detect enough LEDs, the pose of the controller can be determined using elaborate concepts from computer vision \cite{ocControllerTracking}. Contrary, Pico \cite{picoXrCompany} followed a different approach with their Pico Neo 2 Headset. A special electromagnetic emitter is installed in the controller and tracked by a special sensor \cite{electromagnetic_ctrl_trackg}. The controller can then even be tracked if the direct vision of the controller is covered, e.g., by the user's hand. A compatible emitter is also installed in the SenseGloves. 

\subsection{Oculus}
Using the Quest's four front-facing cameras, the software behind the Meta Quest tracks the hand pose. First, it detects the hand within the camera images. Then, the software uses sophisticated Machine Learning algorithms to extract a hand skeleton from the hand. This skeleton extraction is illustrated in \cref{fig:metaHTcameraviews}. One challenge is to use the entire sensor area and not only the center in front of the user \cite{insight_tracking-technology-on-quest,tracking-feel-natural}. For a more natural feel, the entire FOV of all sensors should be used. Furthermore, the AI-Model had to be optimized to run on the limited hardware of the Meta Quest \cite{ChamNetFastEbeddedStuff}.

\begin{figure}[htb]
    \centering
    \includegraphics[width=.7\textwidth]{img/20_system/MetaHTallCameraImages.png}
    \caption{Views from the camera streams of the Meta Quest. (Source \cite{tracking-feel-natural})}
    \label{fig:metaHTcameraviews}
\end{figure}


\subsection{SenseGloves} \label{ssec:SenseGloves}
The SenseGlove Nova is produced by SenseGlove - previously called Adjuvo. It is the second sold generation of haptic feedback gloves. Contrary to the first generation's exoskeleton, the second generation uses a set of strings attached on top of every finger to limit the hand's movement. The elongation of these strings is used at the same time to determine the finger's position w.r.t the base of the device after conducting the calibration (explained in \cref{sec:sg_sdk} and shown in \cref{fig:sc_calibration}). These strings are indicated in  \cref{fig:sgpartsindicated} with the marker \textbf{B}. Indicated with \textbf{A} is the Meta Quest controller mount. It is a 3D-printed part of which the dimensions are well-known to the SDK developer. Based on these dimensions, the transformation from the mounted controller's pose to the hand pose is derived. This approach had to be chosen because the different vendor's hand-tracking is not working reliably enough to track hands with gloves and the clunky device attached to the back. 

\begin{figure}
    \centering
    \includegraphics[width=.7\textwidth]{img/20_system/SenseGlovesParts.png}
    \caption{SenseGlove with attached Meta Quest controller}
    \label{fig:sgpartsindicated}
\end{figure}


\paragraph{Specifications}
\begin{compactdesc}
    \item[Weight:] 385g per glove (540g with attached controller)
    \item[Center of Gravity:] ca 3.5 cm above the user's back of the hand (with controllers attached)  
    \item[Size:] Elastic gloves approx. size 6-9
    \item[Communication:] Bluetooth 4.0
    \item[Battery Life:] approx. 2 hours of usage
\end{compactdesc}


\section{Software}
This section describes the different existing software, programs, and protocols used throughout the Thesis. 

\subsection{Unity Editor} \label{sec:unityEditor}
The SenseGloves SDK (see \cref{sec:sg_sdk}) integrates with the UnrealEngine and the UnityEngine, and so does the Oculus SDK (see \cref{sec:oculus_sdk}). Unity is well-established at \pdz{}, so it was the obvious choice as Game Engine.

Unity is a 2D and 3D Real-Time Game Engine that builds apps for various platforms, including but not limited to Android and PC \cite{UnitySystemReqs}. The Scripting API uses \csharp{} (IL2CPP \cite{UnityIL2CPP} or Mono \texttt{5.X}, version \texttt{9.0} \cite{CSharpCompiler}) as programming language. Under the hood, the editor leverages the concepts of reflection \cite{csharpReflection} and dependency injection \cite{csharpDepInj}, allowing for a simple interaction between the programming and the graphical side of game/simulation development. 

The VR app, as well as any other app using a GUI, was created using the Unity Editor \unityversion. To create the VR app, Unity emits C++ code from its intermediate language  (IL) \cite{UnityIL2CPP}, which is then compiled to the respective platform, i.e., Oculus Quest 2 in this case. 

\subsection{OpenXR}\label{sec:OpenXR}
As seen in \cref{sec:theMetaQuest}, there exist many different vendors of VR and AR systems as well as various game engines (e.g., Unity, Unreal, WebXR). That is why the Khronos Group - well known for their Vulcan standard -  developed OpenXR: "OpenXR is a royalty-free, open standard that provides high-performance access to Augmented Reality (AR) and Virtual Reality (VR) [\dots]" \cite{KhronosOpenXR}. As a standard interface, it prevents so-called "XR-fragmentation," meaning that \textit{not} every vendor of XR-System has to provide bindings to all supported game engines or vice versa.

\subsection{Oculus SDK}\label{sec:oculus_sdk}
The Oculus Integration SDK \cite{OculusSDK} provides all the needed functionality to get the VR app to run. It mainly contains the OVR interaction rig, a large prefab that contains models for the hands and the controller, and, most importantly, the logic to process the controller and hands input from the underlying operating system. Furthermore, it contains all the needed logic that paves the ground to make objects pokeable and interactable. 


\subsubsection{Link Cable}\label{ssec:linkcable}
The Meta Quest Link-Cable shown in \cref{fig:LinkCable} is a 5 meter long USB 3.0 Type C to Type C cable which allows for high bandwidth communication with the computer. It is an essential tool when playing computationally demanding games or - how it is used in this thesis - to use as a debug device with the Unity Editor. The frames are then calculated on the computer and pushed to the headset while returning the hand-tracking or controller pose to the game engine. This setup dramatically increases the development speed since the game can be tested directly without building the Android app.

\begin{figure}[htb]
    \centering
    \includegraphics[width=.5\textwidth]{img/20_system/LinkCable.jpg}
    \caption{The Link Cables, Source: \cite{QuestLinkCable}}
    \label{fig:LinkCable}
\end{figure}

\subsubsection{The Hand Pose Recorder}
For the Oculus SDK to display realistic hand poses when interacting with the object in the 3D World, said hand pose needs to be defined beforehand. When moving the hand close to an interactable object, the quest calculates an error w.r.t. all defined hand poses. If such an error falls below a certain threshold, the displayed hand is snapped to said pose, and the object is "attached" to the tracked hand. \Cref{fig:handposedef} depicts the process of defining such a pose. The small circles are all customizable poses of some link of the hand skeleton. As imaginable when looking at \cref{fig:handposedef} the process of creating a natural-looking hand pose is rather tedious - every blue circle represents one joint of the hand to be placed manually. 

\begin{figure}[htb]
    \centering
    \includegraphics[width=.5\textwidth]{img/20_system/HanposeSelect.png}
    \caption{Defining of a hand pose within Unity}
    \label{fig:handposedef}
\end{figure}

With an update at the beginning of May 2022, Meta released the so-called hand pose recorder, which allows the developer to record these poses within the scene, simplifying the process of creating new interactable objects. Using the unity editor, one can now use hand-tracking to record such poses directly when interacting with the model.

\subsection{SenseGlove SDK}\label{sec:sg_sdk}
SenseGlove provides a Unity-Package \cite{SenseGloveUnity}, which wraps its SenseCom program for unity and integrates with the unity physics system. The SenseCom program communicates with the SenseGloves and provides a first essential calibration (see \cref{fig:sc_calibration}). Based on the measured length of the strings on top of the SenseGloves, the SDK calculates the finger position and maps it to the skeleton of the Unity model. Furthermore, it fuses in the controller pose provided by the Meta Quest's controllers (via OpenXR) to determine the Glove's pose within the 3D world. Leveraging the Unity physics system to detect intersections with other objects' colliders, the Unity Package instructs SenseCom to block the particular movement of the gloves.

\begin{figure}[htb]
    \centering
    \includegraphics{img/20_system/SC_calibration.png}
    \caption{Calibration view of the SenseGlove, Source: \cite{SenseCom}}
    \label{fig:sc_calibration}
\end{figure}

To achieve the behavior mentioned above, the SenseGlove SDK provides a prefab \cite{UnityPrefabs} of custom hands, a \texttt{SG\_User}-prefab which connects to the Meta interaction rig. When starting the app, the \texttt{SG\_User} calibrates the gloves again. Furthermore, the SDK contains a \texttt{SG\_Grabable} and \texttt{SG\_Material} component. Attaching these components to an Object together with a collider \cite{UnityCollider} is enough to make an object grabbable by the SenseGloves. Additionally, the SDK incorporates a variety of scripts that allow for different behaviors not used during this thesis, e.g., pass-through zones, breakable or soft objects, and various helpers for specific movements. 

\paragraph{Note:} Even though SenseGlove SDK uses the OpenXR interface to read the controller's position, it does not publish to that interface again. For that reason, a custom interface to interact with the components from the MRTK \cite{mrtkoverview} or similar libraries was required.


\subsection{Other SDKs and Packages}
The app written through this thesis took advantage of several other packages, SDKs, and Unity Assets. These are the most noteworthy ones: 

\begin{compactdesc}
    \item[NewtonSoft:] Provided two packages: Json.Net \cite{newtonsoftjson} and JsonSchema.Net \cite{newtonsoftjsonschema}. Both libraries interact with \texttt{JSON}s and serialize and deserialize \csharp{} records.
    \item[MRTK:] The Mixed Reality Toolkit \cite{mrtkoverview} contains many out-of-the-box, easy-to-use components that can be dragged into the Scenes. It supports many input systems, but not the one from the SenseGloves, which is why it had to be replaced with custom components for the app created in this thesis.
    \item[Quick Outline:] This Unity Asset \cite{UnityAssetQuickOutline} contains shaders and components which help to outline and highlight the component also through other parts. 
    \item[Workplace Tools:] Another Unity Asset \cite{UnityAssetworkplaceTools} providing e.g. the screwdriver model.
\end{compactdesc}



\subsection{Tree Creator}
During a past thesis at \pdz{},  Mosberger \cite{AntoniaMT} created another Unity app - called \treecreator{} (see \cref{fig:treecreator}). This app allows users to easily edit large decision trees representing complex tasks via a simple GUI. Furthermore, it supports (almost) arbitrary large tree sizes, an automatically generated visualization of the tree while also restricting the user's input always to be safe, w.r.t. the underlying representation, (see. \cref{ssec:treeRep}). However, said representation is not validated upon loading the program of the file. 

The \treecreator{} has been tested several times at the RICO maintenance center and has also proven helpful during this thesis. 

\begin{figure}[htb]
    \centering
    \includegraphics{img/20_system/TreeCreator.png}
    \caption{\treecreator{}, an app to manage decision trees by Mosberger \cite{AntoniaMT}}
    \label{fig:treecreator}
\end{figure}


\subsubsection{Tree Representation} \label{ssec:treeRep}
Currently, the \treecreator{} uses two \texttt{.csv} files plus an optional \texttt{.txt} to represent the data contained within the said tree in a persistent format. One \texttt{.csv} represents the edge, while the other contains the data associated with every node. The underlying principle looks as follows: every node has a Unique Identifier (UID) associated with it, and the edges (which also have UIDs) contain two Node-UIDs, one saved as the source and one saved as the target. A small (incomplete) excerpt is depicted in \cref{lst:edgeRep,lst:verticesRep,lst:iconsRep}. The \texttt{.txt} file contains additional data used for the icon placement by Dietler's AR app \cite{SilasMT}. 
% \clearpage

\begin{lstlisting}[caption={Edge representation in the \treecreator{} (\texttt{edges.csv})}, label=lst:edgeRep, breaklines=true]
NKKD;VHDT;Option;1. Teil;1. Teil;VYGF
NKKD;MPKA;Option;2. Teil;2. Teil;TJRF
NKKD;VTAY;Option;3. Teil;3. Teil;SNMP
\end{lstlisting}

\begin{lstlisting}[caption={Node representation in the \treecreator{} (\texttt{vertices.csv})}, label=lst:verticesRep, breaklines=true]
LUHE;;Kratzer;Untersuche beide Deckel auf Kratzspuren auf der Innenseite.;0;-;-
SZYA;34_Stoessel;DemontageStoessel;Demontiere die Zylinderstoessel an der Seite der Kupplung. Achtung:Federspannung;5|5|8;660-451-678-01ALT_II|660-451-678-01ALT_II;-
PCSB;34_Stoessel;ReinigungStoessel;Die Stoessel muessen mit einem fusselfreien Lappen gereiningt werden. ;0;-;-
\end{lstlisting}



\begin{lstlisting}[caption={Edge representation in the \treecreator{} (\texttt{IconPos.txt})}, label=lst:iconsRep, breaklines=true]
;;NKKD
;-4.983197|24.18348|13.32384|-3.754446|-0.8784923|-0.6386441;OFGR
;;JMDV
-208.4655|-100.8357|-155.1296|0.6310076|0.2593704|0.4006557|-0.6115813|0.7108707|0.7108707|0.1421741$-27.32622|-102.65|-147.7953|0.5013176|0.4934247|0.5369137|-0.4657647|0.4340192|0.4340192|0.4340192;;TPHH
\end{lstlisting}

The idea of this representation builds upon Hutter's work \cite{IanHutterST} and is, in its core idea, well thought out. However, a nest-able File format like \texttt{JSON} or \texttt{yaml} would be better suited for such a task. The above format depicted in \cref{lst:verticesRep} contains nested data that must be encoded in a custom way. This encoding requires a custom serializer and deserializer. Furthermore, these multiple files are hard to manage and validate when reading and not entirely safe if, e.g., any of the texts contain a semicolon (\texttt{;}) needing a custom encoding. \Cref{sec:impltreecreat} suggests an improved representation and implementation. 
