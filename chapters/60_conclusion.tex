\chapter{Conclusion and Outlook}\label{ch:conclusion}
VR shows high potential in maintenance and assembly tasks saving costs and bringing relief to instructors and experts (see \cref{sec:motivation}) as VR applications will more and more supplement such training tasks. Different manufacturers created various devices to increase the immersiveness of such VR apps, comparable to the degree of a purpose-built simulator. One such device is the SenseGloves. This thesis intends to discover the acceptance and chances of such a VR app on the example of coupling maintenance at RICO. To achieve this, the following research questions have been formulated:

\begin{itemize}
    \item \textbf{\cref{rq:ARvsVR}} \rqARvsVRText{}
    \item \textbf{\cref{rq:htSG}} \rqhtSGText{}
    \item \textbf{\cref{rq:neededexactness}} \rqneededexactnessText{}
    \item \textbf{\cref{rq:acceptance}} \rqacceptanceText{}
\end{itemize}

Answering all these research questions requires a working VR app that supports both input systems: the haptic feedback gloves and optical hand-tracking. These requirements led to creating the proof of concept app described in \cref{ch:implementation}. This app is still in a prototype phase, which will be discussed further in \cref{sec:outlook}. However, the main concepts described in \cref{ch:implementation} can be reused in any other app independent of the concrete application.

\section{Conclusion}


\paragraph{\cref{rq:ARvsVR}}
Answering this question is rather complex since the missing understanding of the overall system mentioned in Mosberger \cite{AntoniaMT} has not been quantified during the respective thesis. As such, the VR app's success in teaching an understanding of the overall system cannot be concluded in comparison with the AR app. The answers received during the user study appear to be positive that the VR app helps the technicians understand the overall system; mainly since the user can freely explore the system by themselves but can also be guided through the sequence of tasks.

\paragraph{\cref{rq:htSG}}
This question divided the participant's opinions the most. For most of them, the precision of the optical hand-tracking and the additional feedback of the SenseGloves is a tradeoff. Apart from one participant, they agreed that the Meta Quest's hand-tracking is more precise. However, some were willing to accept a loss in precision for better feedback, i.e., feeling when an object has been grabbed. The answer to the better-suited input system is primarily up to the participant's preference, and the exact reasons could not be determined.  

\paragraph{\cref{rq:neededexactness}}
The participants all agreed that more visual details are desirable. The visualization as well as the interaction with any objects are satisfactory. However, the VR app would greatly profit from an entire model of the train and maintenance pit. On the other side, the majority of participants would e.g., replace the simulation of the screw for a "{}magnetic"{} screwdriver. The user must learn to use the correct tool; the actual simulation thereof is unimportant to them. Generally, a more realistic visual representation is preferred over an accurate simulation of the actual tool interactions. 

\paragraph{\cref{rq:acceptance}}
Generally, the acceptance was high among all participants. Everyone agreed that such an app would help them to learn a new module. Steps can be learned hands-on and do not need to be studied from a paper manual or based on some oral instruction. They do not fear breaking anything and can - given an exact enough model - try out various sequences of steps and learn the effects hands-on. The maintenance workers all wish for more standardized instructions in the training. They are convinced that this can be achieved via such a VR app while maintaining more interactability compared to, e.g., a video.

\section{Outlook}\label{sec:outlook} 
Given the time constraints, not every feature of the VR app could be implemented, and the scope of the user study had to be reduced. The following sections suggest further steps, given the current state of the work. 

\subsection{User Study}
This thesis' user study focused on the acceptance and usability of the app as well as the comparison of the two input systems: Haptic feedback gloves and optical hand-tracking. Another attractive area is to evaluate the performance of such an app as a means of teaching: Does the maintenance worker internalize the required steps faster than more conventional methods? In other words, quantify the learning curve of a worker to be trained on a new module over several training sessions and, e.g., compare the training success to the advancement of a user using a conventional method. 

\subsection{Virtual Reality App}
While the VR app fulfilled its purpose to answer the research questions (see \cref{sec:ReasearchQuestion}), it is very much a proof of concept. As such, the app has to be developed further before it can be used as a complete training app:

\begin{itemize}
    \item As mentioned in \cref{sec:stateofanim}, not every step has been animated fully. Once completed, the full preemptive checkup can be simulated within the VR app.
    \item Interestingly, the maintenance workers prefer a more realistic visual representation over the actual interactions, which means that better models of the surroundings and the train have to be acquired for the future of this- or similar VR apps. Furthermore, another component has to be created which can be used by magnetic tools to remove, for example, screws, nuts, and bolts as described in \cref{ssec:ResSimScrew}.
    \item Implementing the above step would also significantly remedy the "{}game-like"{} feeling the app currently has. Introducing a realistic background, optimally a 3D scan from within with the maintenance center or CAD model depicting a realistic scenery, would also help. Future work is required to reduce the app's game-like feeling explicitly.
    \item The participants requested additional feedback to display when an object has been grabbed: e.g., by coloring the hand models differently when gripping an object.
    \item Currently, the reports generated by the app are relatively sparse. Extending them will provide better insights which can be used to give feedback to the worker and improve the app.
\end{itemize}

Generally, one can focus more on modeling more parts of the workflow and the visualization over the simulation of accurate interactions. As described in Bo et al. \cite{Bo_2012}, this app suffers from low generality and low expandability because such training apps have a relatively large amount of process- and model-specific components and are hard to generalize. While the model-specific parts are barely applicable to a new maintenance task on a new model, this thesis provides an environment that a future developer can easily integrate into a different training app involving a mechanical system. This environment includes but is not limited to the snappable logic, the assembly rules, the scenario editor, the decision trees, etc.

Given the overwhelmingly positive feedback from the technicians, there is much potential in the VR app created during this thesis. Most of the above improvements can be implemented relatively quickly and help set the priorities for further development. With reasonable effort, the app can be brought beyond the proof-of-concept state and become a complete and extensive teaching aid for coupling maintenance that can be evaluated during an extensive user study.  

