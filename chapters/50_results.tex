\chapter{Results}\label{ch:results}

This chapter presents the results from the user studies conducted as described in \cref{ch:methods}. The raw data and the summary of the open questions can be found in the \cref{tab:userStudyupper,tab:userStudylower,tab:openquestionstable1,tab:openquestionstable2} in \cref{app:userstudy}.

\section{Testing environment}\label{sec:testingenv}
A total of $N=9$ maintenance workers took part in the user study at RICO conducted over two days. As per Nielsen \cite{NielsenFivePeopleWeb}, this suffices to get most of the information for such a qualitative study. Nielsen et al .\cite{NielsenFivePeoplepaper} show that the best benefit/cost ratio for a usability study of "{}medium"{} size is at $N_{opt} = 4$ while the maximum $N$ worth its cost is at $N_{max} = 16$. As this study is relatively small, and $N$ is between the two values, it is reasonable to expect meaningful insights.

On the first day of the study, the setup described in \cref{fig:studysetuphardware} worked as intended. On the second day, there was an issue with the Bluetooth connection from the PC to the gloves. That is why the last four entries in \cref{tab:userStudyupper,tab:userStudylower,tab:openquestionstable1,tab:openquestionstable2} are done with a slightly different setup: The app was compiled to the Meta Quest and run on the headset while connecting the gloves to the headset via Bluetooth. This lead to some drops in the frame rate as described in \cref{sec:performance}. However, this issue went largely unnoticed.

To keep the impact for RICO as low as possible, most workers solved Part 1 and 2 only partially. The minimum all workers did is indicated in \cref{fig:SolvedTree}. These steps have enough similarity to give the user the possibility to compare some interactions with the SenseGloves and the optical hand-tracking directly while still being a "{}real"{} scenario.

\begin{figure}[htb]
    \centering
    \includegraphics[width=.8\textwidth]{img/50_results/SolvedTree.png}
    \caption{The minimum tasks solved by each worker, \treesource{}}
    \label{fig:SolvedTree}
\end{figure}

\section{Processing Time}
The times each participant took to perform the particular task are listed in \cref{tab:processingtimes}. For clarity, the average times achieved with the meta quest's optical hand-tracking (Occ) and the Sensegloves (SG) is indicated within the table.

\begin{table}[htb]
    \centering
    \begin{tabular}{l|rrr|}
        \cline{2-4}
         & \multicolumn{3}{c|}{Averages} \\ \cline{2-4} 
         & \multicolumn{1}{l|}{Overall} & \multicolumn{1}{l|}{\osgroup{}} & \multicolumn{1}{l|}{\sogroup{}} \\ \hline
        \multicolumn{1}{|l|}{\textit{Looking Around}} & \multicolumn{1}{r|}{0:01:18} & \multicolumn{1}{r|}{(Occ) 0:01:42} &(SG) 0:01:05 \\ \hline
        \multicolumn{1}{|l|}{Solve Task 1} & \multicolumn{1}{r|}{0:07:16} & \multicolumn{1}{r|}{(Occ) 0:06:31} & (SG) 0:07:33 \\ \hline
        \multicolumn{1}{|l|}{Solve Task 2} & \multicolumn{1}{r|}{0:04:50} & \multicolumn{1}{r|}{(SG) 0:06:46} & (Occ) 0:04:39 \\ \hline
    \end{tabular}
    
    \caption{Processing Time for the two tasks}
    \label{tab:processingtimes}
\end{table}

It is obvious that solving the second task is faster than solving the first: using the SenseGloves, the first task takes 7:33 while the second takes 6:46 and respectively 6:31 for the first and 4:39 for the second task using the optical hand-tracking. Similarly, this is visible in the overall average. Also, the users generally have longer when using the SenseGloves independent of the task. 

\section{Short Questions}
The answers to the short questions are found in \cref{tab:numericquestions}. The answers are split into two columns: the answer for the SenseGloves and the optical hand-tracking, for which the average and the standard deviation are calculated. The column \textbf{Comp.} represents the difference between the average of the SenseGloves and the averages of the optical hand-tracking. A positive \textbf{Comp.} value shows that SenseGloves' value is higher and vice-versa.

\newcommand{\studyquestion}[1]{\textbf{Q{#1}}}

\begin{table}[htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \begin{tabular}{llccccr}
            \cline{3-7}
             & \multicolumn{1}{r|}{} & \multicolumn{2}{c|}{\textbf{SenseGloves}} & \multicolumn{2}{c|}{\textbf{Opt. HT}} & \multicolumn{1}{l|}{\textbf{Comp.}} \\ \cline{3-7} 
             & \multicolumn{1}{l|}{\textbf{}} & \multicolumn{1}{c|}{Avg} & \multicolumn{1}{c|}{STD} & \multicolumn{1}{c|}{Avg} & \multicolumn{1}{c|}{STD} & \multicolumn{1}{l|}{SG-HT} \\ \cline{2-7} 
            \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{\textbf{Operation}} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} \\ \hline
            \multicolumn{1}{|l|}{\studyquestion{1}} & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}I found the system \\ unnecessarily complex.\end{tabular}} & \multicolumn{1}{c|}{1.94} & \multicolumn{1}{c|}{0.90} & \multicolumn{1}{c|}{1.72} & \multicolumn{1}{c|}{0.83} & \multicolumn{1}{r|}{0.22} \\ \hline
            \multicolumn{1}{|l|}{\studyquestion{2}} & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}The interactions in the VR app depicts a \\ realistic presentation of the steps to be executed.\end{tabular}} & \multicolumn{1}{c|}{3.67} & \multicolumn{1}{c|}{0.69} & \multicolumn{1}{c|}{3.44} & \multicolumn{1}{c|}{1.01} & \multicolumn{1}{r|}{0.22} \\ \hline
            \multicolumn{1}{|l|}{\studyquestion{3}} & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}The VR app helps me to build up a \\ understanding for the system "Coupling".\end{tabular}} & \multicolumn{1}{c|}{4.67} & \multicolumn{1}{c|}{0.25} & \multicolumn{1}{c|}{4.56} & \multicolumn{1}{c|}{0.53} & \multicolumn{1}{r|}{0.11} \\ \hline
             &  & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} \\ \cline{2-2}
            \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{\textbf{Usability}} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} \\ \hline
            \multicolumn{1}{|l|}{\studyquestion{4}} & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}The VR app helps me to imagine and learn the \\ sequence of tasks and the diagnosis steps.\end{tabular}} & \multicolumn{1}{c|}{4.50} & \multicolumn{1}{c|}{0.63} & \multicolumn{1}{c|}{4.50} & \multicolumn{1}{c|}{0.79} & \multicolumn{1}{r|}{0.00} \\ \hline
            \multicolumn{1}{|l|}{\studyquestion{5}} & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}I could imagine to use the VR app \\ for (coupling) training.\end{tabular}} & \multicolumn{1}{c|}{4.83} & \multicolumn{1}{c|}{0.13} & \multicolumn{1}{c|}{4.83} & \multicolumn{1}{c|}{0.35} & \multicolumn{1}{r|}{0.00} \\ \hline
            \multicolumn{1}{|l|}{\studyquestion{6}} & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}It is sensible to use a VR app for a \\ different training.\end{tabular}} & \multicolumn{1}{c|}{4.89} & \multicolumn{1}{c|}{0.11} & \multicolumn{1}{c|}{4.89} & \multicolumn{1}{c|}{0.33} & \multicolumn{1}{r|}{0.00} \\ \hline
            \multicolumn{1}{|l|}{\studyquestion{7}} & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}I think this app would be useful \\ for the first months at RICO.\end{tabular}} & \multicolumn{1}{c|}{4.50} & \multicolumn{1}{c|}{0.50} & \multicolumn{1}{c|}{4.50} & \multicolumn{1}{c|}{0.71} & \multicolumn{1}{r|}{0.00} \\ \hline
             &  & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} \\ \cline{2-2}
            \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{\textbf{Cognitive Load}} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} & \multicolumn{1}{l}{} \\ \hline
            \multicolumn{1}{|l|}{\studyquestion{8}} & \multicolumn{1}{l|}{Using the VR app is mentally demanding.} & \multicolumn{1}{c|}{1.56} & \multicolumn{1}{c|}{0.78} & \multicolumn{1}{c|}{1.78} & \multicolumn{1}{c|}{0.97} & \multicolumn{1}{r|}{-0.22} \\ \hline
            \multicolumn{1}{|l|}{\studyquestion{9}} & \multicolumn{1}{l|}{While performing the task, I did feel stress.} & \multicolumn{1}{c|}{1.06} & \multicolumn{1}{c|}{0.03} & \multicolumn{1}{c|}{1.00} & \multicolumn{1}{c|}{0.00} & \multicolumn{1}{r|}{0.06} \\ \hline
            \multicolumn{1}{|l|}{\studyquestion{10}} & \multicolumn{1}{l|}{While performing the task, I did feel frustration.} & \multicolumn{1}{c|}{2.44} & \multicolumn{1}{c|}{1.34} & \multicolumn{1}{c|}{2.00} & \multicolumn{1}{c|}{1.00} & \multicolumn{1}{r|}{0.44} \\ \hline
            \multicolumn{1}{|l|}{\studyquestion{11}} & \multicolumn{1}{l|}{Accomplishing this task does require hard work.} & \multicolumn{1}{c|}{1.11} & \multicolumn{1}{c|}{0.11} & \multicolumn{1}{c|}{1.11} & \multicolumn{1}{c|}{0.33} & \multicolumn{1}{r|}{0.00} \\ \hline
            \multicolumn{1}{|l|}{\studyquestion{12}} & \multicolumn{1}{l|}{I am satisfied with my performance.} & \multicolumn{1}{c|}{4.5} & \multicolumn{1}{c|}{0.25} & \multicolumn{1}{c|}{4.28} & \multicolumn{1}{c|}{0.67} & \multicolumn{1}{r|}{0.22} \\ \hline
        \end{tabular}
    }
    \caption{Summary of the short questions, $N=9$}
    \label{tab:numericquestions}
\end{table}

The only striking difference in \cref{tab:numericquestions} is \studyquestion{10}. Tendencially, more participants are frustrated when using the SenseGloves, which is also discussed in \cref{ssec:ResSgvsoptHZ}. Overall the participants had a more challenging time grabbing the smaller screws with the Gloves than with optical hand-tracking. 

There are some tendencies for the SenseGloves to depict a more realistic representation, yielding a more satisfying performance while making the system unnecessarily complex. Furthermore, optical hand-tracking tends to be less mentally demanding. However, these differences are rather low.

While answering the questionnaire, most participants stated that there is barely any difference between the input systems w.r.t. \studyquestion{4-7}. %This tendency is directly visible in \cref{tab:numericquestions}.

\section{Open Questions}
The most important statements from the open questions are depicted in \cref{tab:openquestionslearnings}. These will be discussed in \cref{ssec:ResSgvsoptHZ,ssec:ResSimScrew}. The rest of the answers to the open questions found in \cref{tab:openquestionstable1,tab:openquestionstable2} will then be discussed in the subsequent sections.

\begin{table}[htb]
    \centering
    \begin{tabular}{l|ll|}
        \cline{2-3}
         & \multicolumn{2}{l|}{Counts} \\ \hline
        \multicolumn{1}{|l|}{SenseGloves vs. optical hand-tracking} & \multicolumn{1}{l|}{4x HT} & 5x SG \\ \hline
        \multicolumn{1}{|l|}{Simulation of the Screw} & \multicolumn{1}{l|}{2x Yes} & 7x No \\ \hline
    \end{tabular}
    
    \caption{Learnings from Open Questions}
    \label{tab:openquestionslearnings}
\end{table}

\subsection{SenseGloves vs. Optical Hand-Tracking} \label{ssec:ResSgvsoptHZ}
The first row of \cref{tab:openquestionslearnings} indicates the participants' feelings towards \cref{rq:htSG}. The SenseGloves splits the group of participants, 5 out of the 9 technicians prefer the SenseGloves over optical hand-tracking. Most agree that the optical hand-tracking feels more exact than the SenseGloves. However, some would be willing to trade some of the precision for more feedback. The participants especially liked having feedback when an object was grabbed. Interestingly, one participants felt more precise when using the SenseGloves, despite them only estimating the finger poses.

\subsection{Simulation of the Screw} \label{ssec:ResSimScrew}
Most participants agree that the simulation of the screw, to that extent, is neither very accurate nor needed. The simulation in that regard was more precise with the SenseGloves but still needs to be more accurate. However, removing the screw using a screwdriver was one of the leading causes of frustration in \studyquestion{10}. Most of them expressed that the simulation would greatly profit from a "{}magnetic"{} screwdriver, with which it suffices to touch a screw to remove it altogether. Such a behavior of the screwdriver forces the technician to use the correct tool without causing much hassle. "{}Everyone knows how to operate a screwdriver"{}. This is the first indication towards \cref{rq:neededexactness}.


\subsection{VR vs. AR} \label{ssec:vsAR}
Whether the VR app conveys a better understanding of the overall system compared to the AR app (see \cref{rq:ARvsVR}) is the most challenging question to answer. While the apps created previously \cite{AntoniaMT,SilasMT} aim at creating an interactive manual in the first place, the VR app focuses on being a training application. The two participants who already used the AR app during Mosberger's\cite{AntoniaMT} and Dietler's \cite{SilasMT} studies found it rather hard to compare the two apps as there was about half a year between the studies. As such, the answers to the open question number 1 (see \cref{ssec:openquestions}) and the \studyquestion{7} conclude that the app in and of itself can help the users understand the system. However, this effect cannot be quantified, given the current data. 

\subsection{Usability and Feeling in VR} 
This section contains additional observations to the ones conducted in \cref{ssec:ResSimScrew}. Generally, the users liked the interactions in the VR world and found themselves familiar within the VR world. They liked how the model and the tools looked, how they could be interacted with, and simply moving around the coupling in VR for exploration. However, some points were mentioned several times: The current feedback when grabbing an object could be more explicit, especially the subtle change in color of the fingers of the Meta Quest's hand-tracking did not suffice. As such, they requested a clearer indication. Also, the grabbing motion has to be done much more exaggeratedly compared to what is required in the real world. The technicians also wish for a more realistic scenery: currently, the coupling hovers over a small floor area. A model of a complete train, as well as a realistic room layout, would improve the realism of the app.

\subsection{Usage in other Areas} \label{ssec:usageinotherareas}
The participants unanimously agreed that such a VR app would be desirable not only for the coupling but also for other modules. Most often mentioned were the doors and toilets and the pivoted bogie (\textit{Drehgestell}). However, such a VR app is not needed for the more straightforward tasks, e.g., the T-77 Heating-Module, as this primarily consists of cleaning. There, a video would suffice. Another interesting feedback was that such an app is useful for learning new modules, even for senior technicians. On the other hand, some participants raised concerns that such an app is not well suited for diagnosis tasks as there is too much variance in the process.

\subsection{Standardization}
Sentences like "{}No-one tells you exactly what you have to do {[when tackling a broken module]}"{} or "{}One {[expert]} explains the {[task]} in this way, the other the other way around"{} were expressed several times during the user study. The participants are convinced that the VR app can remedy these problems, confirming our presumption that these diagnosis tasks are not standardized and could greatly profit from such an app. Also, one participant mentioned that such an app might even be interesting for senior coworkers who had to learn a new module or - once delivered - for the new coupling generation, which will be installed sooner or later.

\subsection{Other Chances} \label{ssec:OtherChances}
The participants observed that the training app does not require any extra hardware. Thus, the VR app would be useful for training a new module during downtime while waiting for new vehicles to arrive. Also, they realized that this can be done without any experts in the room or just one overseeing several trainees at once through a central system. One participant mentioned that he prefers to learn something directly by doing it instead of receiving the instructions orally and then remembering them again once standing in front of the hardware.


\subsection{Room for Improvement}
The participants suggested several improvements. These all give further answers to \cref{rq:neededexactness}:
\begin{compactitem}
    \item Display more parts of the train, mainly the driver's cab and the front of the train but also the maintenance pit.
    \item Also include a simulation - or at least display - the security-relevant parts. E.g., the steps and gaps between the train and the pit encountered when entering the train.
    \item Improve on the gripping motion. At the moment, one has to perform all gestures too exaggeratedly. 
    \item A "{}magnetic"{} screwdriver (as already mentioned in \cref{ssec:ResSimScrew})
    \item In its current state, the training app feels too much like a game. Adding a more realistic surrounding might remedy this.
    \item One participant of the second day of user studies complained about some drops in the frame rate.
\end{compactitem}


\section{Discussion}
As mentioned in \cref{ssec:participants}, this study covers most of the different employees at RICO. However, given the limited number of participants, no demographic statements can be made. For most people, this was the first time they had ever used a VR headset, and no one had ever used haptic feedback gloves. Hence, they all were fascinated to see a system from their everyday life in virtual reality. To find a true accurate answer concerning the participant's acceptance (\cref{rq:acceptance}), several more sessions are required until the effect of \textit{having a new tool at hand} wears off.

The exact processing times (see \cref{tab:processingtimes}) need to be taken with a grain of salt as about half of the users deviated partially from the instructions in the manual and needed to be returned to the correct steps. However, this should be fine concerning the general information.

\paragraph{SenseGloves vs. Optical Hand-Tracking}
While implementing the VR app, a high effort was put into creating interactions as accurately as possible - apart from the apparent haptic feedback and the different calculations of the finger positions (see \cref{ssec:SenseGloves}). However, as mentioned in \cref{ssec:sg_grabableoverwork,ssec:extendingsggrabable}, the SenseGloves have their input system with a different variant of interacting with objects. For example, restricting the object's movement works more directly with the SenseGloves than the Meta Quest's hand-tracking (see \cref{ssec:extendingsggrabable}). As such, it might have a slight advantage in usability. Also, the points where the objects can be grabbed are not calculated similarly. For large hands (approximately above size 10), the SenseGloves do not work as reliably as for users with smaller hands since the strings are not guided optimally over the fingers. These are relatively minor effects that will not influence the answers too much.
