\chapter{Automatic Train Coupling}

In the same way as the previous theses \cite{AntoniaMT, SilasMT, IanHutterST}, this thesis concentrates on the coupling subsystem at the very front of the train, which will allow some comparisons to the previous work. As seen in \cref{sec:themechsystem}, the coupling is astoundingly complex given its size while still being a well-defined system. Furthermore, the aforementioned theses laid the critical groundwork by working out the sequence of tasks and the difficult decisions of some of the functions (see \cref{sec:premtivetask}) by consulting all involved coupling experts at RICO. Out of quite different approaches, a decision tree was synthesized upon which all experts agree \cite[p.~18]{AntoniaMT}. 

\section{The System}\label{sec:themechsystem}
The train coupling used in this context has been manufactured by Schwab Verkehrstechnik AG, a subsidiary of Faiveley Transport Schwab AG, which in and of itself is a subsidiary of Westinghouse Air Brake Technologies Corporation (wabtec \cite{wabteccorp}). The coupling system bares many similarities to the \textit{Type 10 Automatic Coupler} \cite{wabteccorp10couopler}. The existing system is displayed in \cref{fig:couplin_manual} and can be subdivided into two parts: The mounting rig and shock absorber indicated with \textbf{A} and the coupling head indicated with \textbf{B}. 

The diagnosis task limits itself to the coupling head as it contains many electronic and movable parts which can fail often. As such, any further description will be limited to that section. Its task is, first and foremost, to connect two Trains allowing one to pull the other. The coupling head establishes a mechanical and pneumatic connection during the coupling process. Said process is controlled using a magnetic sensor on the coupling head. Then, on both sides, a drawer is opened and extended until an electrical connection is established. This connection is needed to ensure communication between the two vehicles. The coupling contains heating elements within every movable part to guarantee functionality in all temperatures, adding further complexity to the system.

\begin{figure}[htb]
     \centering
     \begin{subfigure}[htb]{0.49\textwidth}
         \centering
        \includegraphics[width=\textwidth]{img/10_Intro/couplingManual.png}
        \caption{Shock absorber and coupling head (Source \cite{CouplingManaual})}
        \label{fig:couplin_manual}
     \end{subfigure}
     \hfill
     \begin{subfigure}[htb]{0.49\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/10_Intro/eine-kupplung-eines-sbb-flirt.jpg}
         \caption{Installed coupling (Source \cite{OsterZugfoto})}
         \label{fig:couplin_mounted}
     \end{subfigure}
\end{figure}

\Cref{fig:couplin_right,fig:couplin_left} show the outside of the coupling. The most important components are indicated in the following: 
\begin{compactdesc}
    \item[A] Electronics Box
    \item[B] Drawer with Electric Connectors (covered)
    \item[C] Pneumatic Coupling
    \item[D] Pneumatic (Cylinder-) Plunger
    \item[E] Mechanical Interlock, incl. Latch
    \item[F] Top Cover
    \item[G] Indicator of Interlock State, plus Manual Override
    \item[H] Magnetic Sensor
\end{compactdesc}
 
\begin{figure}[htb]
     \centering
     \begin{subfigure}[htb]{0.49\textwidth}
         \centering
        \includegraphics[width=\textwidth]{img/10_Intro/couplingSideRight.png}
        \caption{"{}Electronics"{} side}
        \label{fig:couplin_right}
     \end{subfigure}
     \hfill
     \begin{subfigure}[htb]{0.49\textwidth}
         \centering
         \includegraphics[width=\textwidth]{img/10_Intro/couplingSideLeft.png}
         \caption{"{}Pneumatics"{} side}
         \label{fig:couplin_left}
     \end{subfigure}
     \caption{Important parts of the coupling}
     \label{fig:CouplinParts}
\end{figure}

The coupling head consists mainly of the four subsystems discussed in the following. The parts from \cref{fig:CouplinParts} can be associated with one of the subsystems:

\begin{description}
    \item[Mechanical Interlock:] Connecting two vehicles is the core functionality of the coupling and is accomplished via a clever mechanical system. It involves a self-locking latch that can only be opened under light load using the M2-Motor. Said motor is installed in the cast-iron part of the upper coupling under the top cover. The state of the interlock is indicated with a square axle. In case of a malfunction, it can be manually overridden using an appropriate tool.
    \item[Pneumatics:] There are two pneumatic couplers - one for inflow and the other for outflow. Once the mechanical connection is established (the pneumatic coupling is now sealed), the cylinder plunger is retracted, fusing the pneumatic circuits of the two vehicles. The pressurized air is directed toward the locomotive using high-pressure hoses.
    \item[Heating:] The heating system has to cover and prevent every movable part from freezing, including the latch, interlock, inner system(e.g., M2 motor), pneumatic system, and electronics drawer with a removable cover. The upper part of the coupling head is heated using several cartridges. The drawer is covered with a heating mat on its lower side. 
    \item[Electric System:] This system can be subdivided into two main parts. The electronics box distributes the signals used to control the coupling itself. Within that, there are connections to the M2-Motor, magnet sensors, heating system, and the linear motor for the drawer. The drawer houses more PCBs and a big connector with 40 pins and 40 sockets, allowing communication between two trains. Once the mechanical interlock has been established, the two drawers are extended, the cover (covering the pins) is retracted, and an electric connection is established.
\end{description}

\section{Preemptive Coupling Checkup}\label{sec:premtivetask}
The exact sequence of this task was worked out in previous theses at \pdz{} \cite{SilasMT, AntoniaMT}. Its goal is to detect an error in the coupling before it occurs. 
Preemptively, various subsystems of the coupling are inspected, several manipulations are conducted, and measurements are taken which might indicate a damaged part that will cause a failure. This task bears close similarity with the \textit{Clutch Trouble Shooting} task \cite{SilasMT, AntoniaMT}. The troubleshooting task aims to localize the exact error once it occurs. Most interactions to detect a broken part are the same as those to detect a damaged part. As such, the \textit{Preemptive Coupling Checkup} is a more concise version of the Trouble Shooting tasks with less specific ends. In the troubleshooting task, 17 different endings indicate a component's specific defect, and various additional steps pinpoint the exact part. 

These two tasks share many interactions with the coupling model, use the same tools, and have many congruent steps. For that reason, the \textit{Preemptive Coupling Checkup} task was used during this thesis as it is the task with which a new maintenance worker will come into contact first when learning a new module.

\Cref{fig:preemtiveTreeTask} shows the decision tree for the \textit{Preemptive Coupling Checkup} - a more extensive, unobstructed version of that picture can be found in \cref{fig:completeTree}. For convenient navigation, this tree has been subdivided into three parts which all lead again to a
central "hub."  The single subsystems are indicated in color as follows:

\definecolor{pneumaticColor} {RGB} { 82, 166, 231}
\definecolor{heatingColor}   {RGB} {255, 149,  26}
\definecolor{infoColor}      {RGB} {128, 128, 128}
\definecolor{shortCircColor} {RGB} {233,  50,  78}
\definecolor{setupColor}     {RGB} { 38, 196,   0}
\definecolor{MechlinkColor}  {RGB} {255, 111, 255}
\definecolor{CouplProColor}  {RGB} {255, 255,   0}
\definecolor{DrawM2MotColor} {RGB} {  0,   4, 242}

\begin{compactdesc}
    \item[\colBox{infoColor}:] Information for the user
    \item[\colBox{setupColor}:] Setup Steps
    \item[\colBox{shortCircColor}:] Short Circuit in the electronics
    \item[\colBox{CouplProColor}:] Coupling Process color
    \item[\colBox{MechlinkColor}:] Mechanical Linkage
    \item[\colBox{DrawM2MotColor}:] Electronics drawer and M2 Motor
    \item[\colBox{heatingColor}:] Heating System
    \item[\colBox{pneumaticColor}:] Sequence setup
\end{compactdesc}

\begin{figure}[htb]
    \centering
    \includegraphics[width=\textwidth]{img/10_Intro/PreemtiveTask.png}
    \caption{Decision Tree of the \textit{Preemptive Coupling Checkup} Task. Source: Created with the \treecreator{} using the data from previous theses \cite{SilasMT, AntoniaMT}.}
    \label{fig:preemtiveTreeTask}
\end{figure}
